---
layout: default
---

Simply bookmark this [link]({{site.data.bookmarklet_uri}}) to install.

Development takes place on [FramaGit.](https://framagit.org/bbjubjub/qrcode-bookmarklet)
